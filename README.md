# unreal assets

standard resources for unreal projects

checkout branch depending on your ersion of unreal:

```
git fetch --all
git checkout -b ue4 origin/ue4
```

or

```
git fetch --all
git checkout -b ue5 origin/ue5
```

for c++ classes, checkout source branch:

```
git fetch --all
git checkout -b source origin/source
```

for python scripts, checkout source branch:

```
git fetch --all
git checkout -b python origin/python
```